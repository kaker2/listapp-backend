import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ListsModule } from './lists/lists.module';
import { AppLoggerMiddleware } from './app-logger-middleware.middleware';
import { ListsSharedModule } from './lists-shared/lists-shared.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { CustomMailerService } from './mailer/mailer.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { FcmModule } from 'nestjs-fcm';
import { CustomFcmService } from './custom-fcm/custom-fcm.service';

import { CustomFcmModule } from './custom-fcm/custom-fcm.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    TypeOrmModule.forRoot({
      autoLoadEntities: true
    }),
    ListsModule,
    ListsSharedModule,
    MailerModule.forRoot({
      transport: {
        host: 'localhost',
        port: 1025,
        ignoreTLS: true,
        secure: false,
        auth: {
          user: process.env.MAILDEV_INCOMING_USER,
          pass: process.env.MAILDEV_INCOMING_PASS,
        },
      },
      defaults: {
        from: '"No Reply" <no-reply@localhost>',
      },
      preview: true,
      template: {
        // dir: __dirname + '/mailer/templates/',
        dir: process.cwd()+'/mailer/templates/',
        adapter: new PugAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: true,
        },
      },
    }),
    EventEmitterModule.forRoot(),
    FcmModule.forRoot({
      firebaseSpecsPath: process.cwd()+'/firebase.spec.json',
    }),
    CustomFcmModule,
  ],
  controllers: [AppController],
  providers: [AppService, CustomMailerService, CustomFcmService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    console.log('__dirname: ', process.cwd()+'/mailer/templates/');
    consumer.apply(AppLoggerMiddleware).forRoutes('*');
  }
}
