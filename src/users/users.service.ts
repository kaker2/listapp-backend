import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly userRepo: Repository<User>, ) {}

    async findManyByIds(ids = [], options = {}) {
      try {
        return await this.userRepo.findByIds(ids, options);
      } catch(err) {
        console.log(err);
      }
    }

    async findOne(options?: object): Promise<User> {
        return this.userRepo.findOne(options);
    }

    async save(options?: object): Promise<User> {
        return this.userRepo.save(
            this.userRepo.create(options)
        );
    }

    async update(id: string, updateData) {
        try {
          var item = await this.userRepo.findOne(id);
          if (!item) {
            return 'Użytkownik nie istnieje';
          }
        } catch(err) {
          console.log(err);
        }

        return this.userRepo.update(
          id,
          updateData
        );
    }

    async softDelete(id?: number) {
        return this.userRepo.softDelete(id);
    }
}
