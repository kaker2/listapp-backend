import { SetCreatedByInterceptor } from './set-created-by.interceptor';

describe('SetCreatedByInterceptor', () => {
  it('should be defined', () => {
    expect(new SetCreatedByInterceptor()).toBeDefined();
  });
});
