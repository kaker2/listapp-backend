import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class CustomMailerService {
    constructor(private readonly baseService: MailerService) {}
    public send(data): void {
      console.log(data, data.shared_url);
        this.baseService
          .sendMail({
            to: data.email, // list of receivers
            from: 'noreply@kakertest.com', // sender address
            subject: 'Lista została udostępniona', // Subject line
            template: 'new-list-shared', // The `.pug`, `.ejs` or `.hbs` extension is appended automatically.
            context: {
              url: data.shared_url,
            }
          })
          .then(() => {})
          .catch(() => {});
      }
}
