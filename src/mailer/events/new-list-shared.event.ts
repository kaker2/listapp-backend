

export class NewListSharedEvent {
    user: {
        user: number,
        email: string
    };
    list: string;
    createdBy: number;
    shared_url: string;
}