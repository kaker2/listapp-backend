import { Injectable } from "@nestjs/common";
import { NewListSharedEvent } from "../events/new-list-shared.event";
import { OnEvent } from '@nestjs/event-emitter';
import { CustomMailerService } from "../mailer.service";
import { ListsSharedService } from "src/lists-shared/lists-shared.service";

@Injectable()
export class NewListSharedListener {

    constructor(
        private readonly customMailerService: CustomMailerService,
        private readonly listsSharedService: ListsSharedService
    ) {}

    @OnEvent('list.shared')
    async handleNewListSharedEvent(event: NewListSharedEvent) {
        let data = {
            user_id: event.user.user,
            list: event.list,
            createdBy: event.createdBy
        };

        let item = await this.listsSharedService.findOneWhere(data);

        if (item) {
            let merged = {...data, ...{updated_at: new Date}};
            await this.listsSharedService.update(item.id, merged);
        } else {
            await this.listsSharedService.create(data);
        }
        this.customMailerService.send({...event, ...{shared_url: event.shared_url}});
    }
}