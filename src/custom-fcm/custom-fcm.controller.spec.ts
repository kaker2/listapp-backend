import { Test, TestingModule } from '@nestjs/testing';
import { CustomFcmController } from './custom-fcm.controller';

describe('CustomFcmController', () => {
  let controller: CustomFcmController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CustomFcmController],
    }).compile();

    controller = module.get<CustomFcmController>(CustomFcmController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
