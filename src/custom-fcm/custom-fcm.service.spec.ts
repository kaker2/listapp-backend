import { Test, TestingModule } from '@nestjs/testing';
import { CustomFcmService } from './custom-fcm.service';

describe('CustomFcmService', () => {
  let service: CustomFcmService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomFcmService],
    }).compile();

    service = module.get<CustomFcmService>(CustomFcmService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
