import { Module } from '@nestjs/common';
import { ListsSharedModule } from 'src/lists-shared/lists-shared.module';
import { UsersController } from 'src/users/users.controller';
import { UsersModule } from 'src/users/users.module';
import { CustomFcmController } from './custom-fcm.controller';
import { CustomFcmService } from './custom-fcm.service';

@Module({
    imports: [ListsSharedModule, UsersModule],
    controllers: [CustomFcmController, UsersController],
    providers: [CustomFcmService]
  })
export class CustomFcmModule {

}
