import { Injectable } from '@nestjs/common';
import { FcmService } from 'nestjs-fcm';

@Injectable()
export class CustomFcmService {
    constructor(private readonly fcmService: FcmService) {}

    async sendNotification(tokens: any) {
        const message = {
            data: {
                title: 'Lista została zmieniona',
                body: 'Lista o nazwie została zmieniona',
            }
        };
        await this.fcmService.sendNotification(tokens, message, false);
    }
}
