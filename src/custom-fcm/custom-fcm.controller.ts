import { Body, Controller, Get, Param, Response, UseGuards, UseInterceptors } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ListsSharedService } from 'src/lists-shared/lists-shared.service';
import { SetCreatedByInterceptor } from 'src/users/set-created-by.interceptor';
import { UsersService } from 'src/users/users.service';
import { CustomFcmService } from 'src/custom-fcm/custom-fcm.service';

@Controller('custom-fcm')
@UseGuards(JwtAuthGuard)
export class CustomFcmController {
    constructor(
        private readonly listsSharedService: ListsSharedService,
        private readonly usersService: UsersService,
        private readonly customFcmService: CustomFcmService,
    ) {}

    @Get(':id')
    @UseInterceptors(SetCreatedByInterceptor)
    async send(@Param('id') id: string, @Body() body, @Response() response) {
        let lists = await this.listsSharedService.findManyWhereListId(id);
        let users = [];

        lists.forEach(element => {
            if (element['user_id'] != body.createdBy) {
                users.push(element['user_id']);
            }
            if (element['createdBy'] != body.createdBy) {
                users.push(element['createdBy']);
            }
        });

        users = [4];

        //dorobić pobieraie fcm_token od usera i wysylac im powiadomienia jezeli maja fcm_token nie na null wtedy bedzie to skonczone
        let usersFcmTokenQuery = await this.usersService.findManyByIds(users,  {
            select: ['fcm_token']
        });
        let usersFcmToken = usersFcmTokenQuery.map((row) => row['fcm_token']);
        this.customFcmService.sendNotification(usersFcmToken);

        return response.json({
            users: lists
        });
    }
}
