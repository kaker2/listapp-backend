import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class AppLoggerMiddleware implements NestMiddleware {
  private logger = new Logger('HTTP');

  use(req: Request, res: Response, next: () => void) {
    const { ip, method, headers, originalUrl, body } = req;
    const userAgent = req.get('user-agent') || '';

    res.on('close', () => {
      const { statusCode } = res;
      const contentLength = res.get('content-length');

      this.logger.log(
        `${method} ${originalUrl} Headers: ${JSON.stringify(headers)} Body: ${JSON.stringify(body)} ${statusCode} ${contentLength} - ${userAgent} ${ip}`
      );
    });
    next()
  }
}
