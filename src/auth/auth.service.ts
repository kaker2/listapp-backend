import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
        ) {}

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne({email: email});
        if (user && await bcrypt.compareSync(pass, user.password)) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async saveFcmToken(req: any) {
        try {
            await this.usersService.update(req.user.id, {
                fcm_token: req.body.fcm_token
            })
        } catch(err) {
            console.log(err);
        }
    }

    async login(user: any) {
        const payload = { email: user.email, sub: (user.userId) ? user.userId : user.id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
