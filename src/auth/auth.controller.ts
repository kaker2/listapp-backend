import { ConflictException, Controller, Get, Logger, Post, Request, Response, UseGuards } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { timeStamp } from 'console';
import { UsersService } from 'src/users/users.service';
import { json } from 'stream/consumers';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('auth')
export class AuthController {
    constructor(private usersService: UsersService, private authService: AuthService) {}

    @UseGuards(LocalAuthGuard)
    @Post('/login')
    async login(@Request() req)
    {
        this.authService.saveFcmToken(req);
        return this.authService.login(req.user);
    }

    @Post('/register')
    async register(@Request() req, @Response() response)
    {
        var body = req.body;
        var checkMail = await this.usersService.findOne({
            email: body.email
        });

        if (checkMail !== undefined) {
            throw new ConflictException({
                status: 409,
                lang: "auth_email_found",
                message: 'Taki email już istnieje'
            });
        }

        //tu bedzie generowane jwt
        var user = await this.usersService.save(req.body);

        if (user) {
            return response.json({
                email: user.email
            });
        }
        return null;
    }

    @UseGuards(JwtAuthGuard)
    @Get('profile')
    getProfile(@Request() req)
    {
        return req.user;
    }
}
