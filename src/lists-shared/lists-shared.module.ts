import { Module } from '@nestjs/common';
import { ListsSharedService } from './lists-shared.service';
import { ListsSharedController } from './lists-shared.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ListsShared } from './entities/lists-shared.entity';
import { CustomMailerService } from 'src/mailer/mailer.service';
import { UsersModule } from 'src/users/users.module';
import { NewListSharedListener } from 'src/mailer/listeners/new-list-shared.listener';

@Module({
  imports: [TypeOrmModule.forFeature([ListsShared]), UsersModule],
  exports: [ListsSharedService],
  controllers: [ListsSharedController],
  providers: [ListsSharedService, CustomMailerService, NewListSharedListener]
})
export class ListsSharedModule {}
