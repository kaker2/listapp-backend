import { Test, TestingModule } from '@nestjs/testing';
import { ListsSharedController } from './lists-shared.controller';
import { ListsSharedService } from './lists-shared.service';

describe('ListsSharedController', () => {
  let controller: ListsSharedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ListsSharedController],
      providers: [ListsSharedService],
    }).compile();

    controller = module.get<ListsSharedController>(ListsSharedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
