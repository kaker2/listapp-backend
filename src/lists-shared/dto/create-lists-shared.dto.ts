export class CreateListsSharedDto {
    user_id: number;
    list: string;
    createdBy: number;
}
