import { PartialType } from '@nestjs/mapped-types';
import { CreateListsSharedDto } from './create-lists-shared.dto';

export class UpdateListsSharedDto extends PartialType(CreateListsSharedDto) {
    updated_at: Date
}
