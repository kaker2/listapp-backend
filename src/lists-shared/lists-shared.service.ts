import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { CreateListsSharedDto } from './dto/create-lists-shared.dto';
import { UpdateListsSharedDto } from './dto/update-lists-shared.dto';
import { ListsShared } from './entities/lists-shared.entity';

@Injectable()
export class ListsSharedService {
  constructor(
    @InjectRepository(ListsShared)
          private readonly repo: Repository<ListsShared> ) {}

  create(createListsSharedDto: CreateListsSharedDto) {
    return this.repo.save({
      user_id: createListsSharedDto.user_id,
      createdBy: createListsSharedDto.createdBy,
      list: createListsSharedDto.list
    });
  }

  findAll() {
    return this.repo.find({
      relations: ['list']
    });
  }

  findOneWhere(object: Object) {
    return this.repo.findOne({
      where: object
    });
  }

  async findManyWhereListId(id) {
    try {
      return await getManager().createQueryBuilder('lists-shared','ls')
      .setParameters({ listId: id})
      .where( 'ls.list::text = :listId')
      .andWhere('ls.user_id is not NULL')
      .andWhere('ls.createdBy is not NULL')
      .getMany();
    } catch(err) {
      console.log(err);
    }

  }

  findOne(id: number) {
    return `This action returns a #${id} listsShared`;
  }

  async update(id: string, updateListsSharedDto: UpdateListsSharedDto) {
    console.log(updateListsSharedDto);
    var item = await this.repo.findOneOrFail(id);
    if (!item) {
      return 'Lista nie istnieje';
    }
    return this.repo.update(
      id,
      updateListsSharedDto,
    );
  }

  remove(id: number) {
    return this.repo.delete(+id);
  }
}
