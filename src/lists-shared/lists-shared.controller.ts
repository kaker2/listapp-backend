import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UseGuards, HttpStatus, Response, BadRequestException } from '@nestjs/common';
import { ListsSharedService } from './lists-shared.service';
import { CreateListsSharedDto } from './dto/create-lists-shared.dto';
import { UpdateListsSharedDto } from './dto/update-lists-shared.dto';
import { SetCreatedByInterceptor } from 'src/users/set-created-by.interceptor';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UsersService } from 'src/users/users.service';
import { CustomMailerService } from 'src/mailer/mailer.service';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { NewListSharedEvent } from 'src/mailer/events/new-list-shared.event';
// import { Response } from 'express';

@Controller('lists-shared')
@UseGuards(JwtAuthGuard)
export class ListsSharedController {
  constructor(
    private readonly listsSharedService: ListsSharedService,
    private readonly usersService: UsersService,
    private eventEmitter: EventEmitter2
    ) {}

  @Post()
  create(@Body() createListsSharedDto: CreateListsSharedDto) {
    return this.createMethod(createListsSharedDto);
  }

  createMethod(createListsSharedDto: CreateListsSharedDto) {
    return this.listsSharedService.create(createListsSharedDto);
  }

  @Get()
  findAll() {
    return this.listsSharedService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.listsSharedService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateListsSharedDto: UpdateListsSharedDto) {
    return this.updateMethod(id, updateListsSharedDto);
  }

  updateMethod(id: string, updateListsSharedDto: UpdateListsSharedDto) {
    return this.listsSharedService.update(id, updateListsSharedDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.listsSharedService.remove(+id);
  }

  @Post('share')
  @UseInterceptors(SetCreatedByInterceptor)
  async share(@Body() body, @Response() res) {
    let user = await this.usersService.findOne({
      email: body.email
    });

    if (!user) {
      return res.json({
        lang: "shared_list_success",
        message: "Brak podanego adresu email w bazie"
      });
      // wysyłamy email z prośbą o zainstalowanie aplikacji
    }

    try {
      let newListSharedEvent = new NewListSharedEvent();
      newListSharedEvent.user = {
        user: user.id,
        email: user.email,
      }
      newListSharedEvent.list = body.list;
      newListSharedEvent.createdBy = body.createdBy;
      newListSharedEvent.shared_url = body.shared_url;
      this.eventEmitter.emit('list.shared', newListSharedEvent);
    } catch(e) {
      throw new BadRequestException({
        lang: "auth_email_found",
        message: 'Taki email już istnieje'
    });
    }

    return res.json({
      lang: "shared_list_success",
      message: "Lista została udostępninona"
    });
  }
}
