import { Test, TestingModule } from '@nestjs/testing';
import { ListsSharedService } from './lists-shared.service';

describe('ListsSharedService', () => {
  let service: ListsSharedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ListsSharedService],
    }).compile();

    service = module.get<ListsSharedService>(ListsSharedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
