import { User } from "src/users/user.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('lists-shared')
export class ListsShared {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: null })
    user_id: number;

    @Column({ default: null })
    list: string

    @Column({ default: null, array: true })
    items: string;

    @Column({
        default: null
    })
    createdBy: number;

    @Column({ default: new Date() })
    created_at: Date;

    @Column({ default: new Date() })
    updated_at: Date;

    @Column({ default: null })
    deleted_at: Date;
}
