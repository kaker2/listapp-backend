import { Controller, Get,Response, Post, Body, Param, Delete, UseGuards, UseInterceptors, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { ListsService } from './lists.service';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { SetCreatedByInterceptor } from 'src/users/set-created-by.interceptor';
import { RequestListsDtoConverterPipe } from './pipe/request-lists-dto-converter.pipe';

@Controller('lists')
@UseGuards(JwtAuthGuard)
export class ListsController {
  constructor(private readonly listsService: ListsService) {}

  @Post()
  @UsePipes(new RequestListsDtoConverterPipe())
  @UseInterceptors(SetCreatedByInterceptor)
  create(@Body() createListDto: CreateListDto) {
    return this.listsService.create(createListDto);
  }

  @Get()
  @UseInterceptors(SetCreatedByInterceptor)
  async findAll(@Body() body, @Response() response) {
    let lists = await this.listsService.findAll(body);
    return response.json({
      lists: lists
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.listsService.findOne(id);
  }

  @Put(':id')
  @UsePipes(new RequestListsDtoConverterPipe())
  // @UseInterceptors(SetCreatedByInterceptor)
  async update(@Param('id') id: string, @Body() updateListDto: UpdateListDto) {
    updateListDto.updated_at = new Date();
    return this.listsService.update(id, updateListDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.listsService.remove(id);
  }
}
