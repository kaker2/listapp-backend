import { RequestListsDtoConverterPipe } from './request-lists-dto-converter.pipe';

describe('RequestListsDtoConverterPipe', () => {
  it('should be defined', () => {
    expect(new RequestListsDtoConverterPipe()).toBeDefined();
  });
});
