import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class RequestListsDtoConverterPipe implements PipeTransform {
  transform(body: any, metadata: ArgumentMetadata) {
    if (typeof body.items != "undefined") {
      var items = JSON.parse(body.items);
      body.items = items.map((value, index) => {
        return JSON.parse(value);
      });

    }
    return body;
  }
}
