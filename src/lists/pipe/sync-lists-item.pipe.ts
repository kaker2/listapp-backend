import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class SyncListsItemPipe implements PipeTransform {
  transform(body: any, metadata: ArgumentMetadata) {
    if (typeof body.lists == "string") {
      var lists = JSON.parse(body.lists);
      lists.map((item) => {
        item.items = JSON.parse(item.items);
      })
      body.lists = lists;
    }
    return body;
  }
}
