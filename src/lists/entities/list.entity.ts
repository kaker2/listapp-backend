import { ListsShared } from "src/lists-shared/entities/lists-shared.entity";
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('lists', {
    orderBy: {
        created_at: "DESC",
    }
})
export class List {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: null })
    title: string;

    @Column({ default: null, array: true })
    items: string;

    @Column({
        default: null
    })
    createdBy: number;

    @Column({ default: new Date() })
    created_at: Date;

    @Column({ default: new Date() })
    updated_at: Date;

    @Column({ default: null })
    deleted_at: Date;
}
