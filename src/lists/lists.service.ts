import { Body, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ListsShared } from '../lists-shared/entities/lists-shared.entity';
import { getManager, In, Repository } from 'typeorm';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { List } from './entities/list.entity';
import { identity } from 'rxjs';
import { CustomFcmService } from 'src/custom-fcm/custom-fcm.service';

@Injectable()
export class ListsService {
  constructor(

  @InjectRepository(List)
    private readonly listRepo: Repository<List>,

  @InjectRepository(ListsShared)
    private readonly listSharedRepo: Repository<ListsShared>,
    private readonly customFcmService: CustomFcmService,
    ) {}

  create(createListDto: CreateListDto) {
    return this.listRepo.save(
      this.listRepo.create(createListDto)
    );
  }

  async findAll(body) {
    try {
      return await getManager().createQueryBuilder('lists','l')
      .leftJoin('lists-shared', 'ls', 'ls.list::text = l.id::text')
      .setParameters({ userId:  body.createdBy })
      .where('ls.user_id = :userId')
      .orWhere('l.createdBy = :userId')
      .getMany();
    } catch(err) {
      console.log(err);
    }
  }

  async findOne(id: string) {
    return this.listRepo.findOne(id);
  }

  async update(id: string, updateListDto: UpdateListDto) {
    try {
      var item = await this.listRepo.findOne(id);

      if (!item) {
        return 'Lista nie istnieje';
      }
    } catch(err) {
      console.log(err);
    }

    return this.listRepo.update(
      id,
      updateListDto
    );
  }

  remove(id: string) {
    return this.listRepo.delete(id);
  }
}
