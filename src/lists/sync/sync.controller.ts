import { Body, Controller, Get, Post, Response, UseGuards, UsePipes } from '@nestjs/common';
import { response } from 'express';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ListsService } from '../lists.service';
import { SyncListsItemPipe } from '../pipe/sync-lists-item.pipe';

@Controller('sync')
@UseGuards(JwtAuthGuard)
export class SyncController {
    constructor(private readonly listsService: ListsService) {}
    @Post('lists')
    @UsePipes(new SyncListsItemPipe())
    syncList(@Body() body, @Response() response) {
        body.lists.filter((item) => item.id !== 'null').map((item) => {
            this.listsService.create(item);
        });
        return response.json({
            'message': true
        })
    }

    @Post('get-shared-list')
    async getSharedList(@Body() body, @Response() response) {
        let list = null;
        try {
            list = await this.listsService.findOne(body.id);
        } catch(error) {
            console.log(error);
        }

        return response.json({
            'list': list
        })
    }
}
