import { Module } from '@nestjs/common';
import { ListsService } from './lists.service';
import { ListsController } from './lists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { List } from './entities/list.entity';
import { ListsShared } from 'src/lists-shared/entities/lists-shared.entity';
import { SyncController } from './sync/sync.controller';
import { CustomFcmService } from 'src/custom-fcm/custom-fcm.service';
import { ListsSharedService } from 'src/lists-shared/lists-shared.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([List]),
    TypeOrmModule.forFeature([ListsShared]),
  ],
  controllers: [ListsController, SyncController],
  providers: [ListsService, CustomFcmService, ListsSharedService]
})
export class ListsModule {}
